//This is a test C++ file for our sparkly new version control systems
UINT32 Foo = 123456;  //added additional precision
UINT32 Bar = 67890;
UINT32 FOOBAR = Foo + Bar;
UINT32 BARSTOOL = Bar - Foo;
UINT32 BARITONE  = 2 ^ Bar; //new calculation for baritones
UINT32 BARNONE = Bar-Bar;
